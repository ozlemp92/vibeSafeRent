import React, { Component } from 'react';
import classnames from 'classnames';
import ReactDOM from 'react-dom';

import Grid from '@material-ui/core/Grid';

import ButtonGroup from '@material-ui/core/ButtonGroup';
import {
  TabContent, CardHeader, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
  CardBody, Input, ButtonDropdown, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col
} from 'reactstrap';
import MenuItem from '@material-ui/core/MenuItem';

import Select from '@material-ui/core/Select';
class TabsPage extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      dropdownOpen: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle(tab) {
    this.state.dropdownOpen ? this.setState({ dropdownOpen: false }) : this.setState({ dropdownOpen: true });
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    const options = ['Create a merge commit', 'Squash and merge', 'Rebase and merge'];
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              href="#"
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => {
                this.toggle('1');
              }}
            >
              Kişisel Bilgiler
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              href="#"
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => {
                this.toggle('2');
              }}
            >
              Kredi Talebi
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col xs="12" md="6">
                <Card>
                  <Col sm="8">
                    <h4>Aşağıdaki formu doğru ve eksiksiz doldurun!</h4>
                  </Col>
                  <Col sm="8">
                    <label>TC Kimlik Numarası</label>
                    <input type="text" name="name" />
                  </Col>
                  <br />
                  <Col sm="12">
                    <label style={{ marginLeft: "50px" }}>Cep Telefonu</label>
                    <input type="text" name="name" />

                  </Col>

                  <Col sm="8">
                    <Button style={{ width: "100px" }} size="sm" color="success" block>Devam</Button>

                  </Col>
                </Card>
              </Col>

            </Row>

          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col xs="12" md="6">
                <Card>
                  <Row>

                  </Row>
                  <Col sm="8">
                    <h2>KİMLİK BİLGİLERİ</h2>
                  </Col>
                  
                  <Col sm="8">
                    <label>ADI</label>
                    <input type="text"  style={{ marginLeft: "48px" }} name="name" />
                  </Col>
               
                  <Col sm="12">
                    <label >SOYADI</label>
                    <input  style={{ marginLeft: "18px" }}type="text" name="name" />

                  </Col>
                  <Col sm="12">
                    <label >TC KİMLİK</label>
                    <input type="text" name="name" />

                  </Col>

                </Card>
              </Col>

            </Row>
            <br />
            <br />
            <Row>
              <Col xs="12" md="6">
                <Card>
                  <Col sm="8">
                    <h2>KİŞİSEL BİLGİLER</h2>
                  </Col>


                  <Col sm="8">
                    <label>ÖĞRENİM DURUMU</label>
                    <Select style={{ width: "60%" }}
                      labelId="demo-controlled-open-select-label"
                      id="demo-controlled-open-select"

                    >

                      <MenuItem value={10}>Lise</MenuItem>
                      <MenuItem value={20}>OrtaÖğretim</MenuItem>
                      <MenuItem value={30}>Üniversite</MenuItem>
                    </Select>
                  </Col>
                  <br />
                  <br />


                  <Col sm="8">
                    <label>MESLEK</label>
                    <Select style={{ width: "80%" }}
                      labelId="demo-controlled-open-select-label"
                      id="demo-controlled-open-select"

                    >

                      <MenuItem value={10}>Öğrenci</MenuItem>
                      <MenuItem value={20}>Mühendis</MenuItem>
                      <MenuItem value={30}>Öğretmen</MenuItem>
                    </Select>
                    <br />
                    <br />

                  </Col>
                  <Col sm="8">
                    <label>AYLIK NET GELİR</label>
                    <input  style={{width:"65%"}} type="text" name="name" />

                  </Col>
                      <br />
                    <br />
                  <Col sm="8">
                    <Button style={{ width: "100px" }} size="sm" color="success" block>Devam</Button>

                  </Col>
                  <br />
                 
                </Card>
              </Col>

            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

export default TabsPage;
