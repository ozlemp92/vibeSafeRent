export default {
  top: [
    {
      name: 'Kira İşlemleri',
      icon: 'File',
      children: [
        {
          name: 'Kira Garanti Kredi Başvurusu',
          url: '/safeRent/safe',
        }
      ],
    },
    {
      name: 'Müşteri İşlemleri',
      icon: 'File',
      children: [
      ],
    },
    {
      name: 'Kiracı İşlemleri',
      icon: 'File',
      children: [
        {
          name: 'Ödeme Talimatları',
          url: '/safeRent/paymentOrders',
        },
        {
          name: 'Kredi Başvuru İşlemleri',
          url: '/safeRent/rentApplicationProcedures',
        }
      ],
    },
  ],
  bottom: [
    {
      name: '',
      url: 'https://github.com/NiceDash/Vibe',
  
      external: true,
      target: '_blank',
    },
    {
      name: '',
      url: '/dashboard',
   
    },
  ],
};
